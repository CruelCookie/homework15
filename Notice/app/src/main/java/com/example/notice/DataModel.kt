package com.example.notice

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class DataModel: ViewModel() {
    val roomContactsRepository: MutableLiveData<RoomContactsRepository> by lazy {
        MutableLiveData<RoomContactsRepository>()
    }
    val names: MutableLiveData<ArrayList<String>> by lazy {
        MutableLiveData<ArrayList<String>>()
    }
    val numbers: MutableLiveData<ArrayList<String>> by lazy {
        MutableLiveData<ArrayList<String>>()
    }
    val currentContact: MutableLiveData<Contact> by lazy {
        MutableLiveData<Contact>()
    }
}
