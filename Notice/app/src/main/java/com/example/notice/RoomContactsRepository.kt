package com.example.notice

import android.accounts.AuthenticatorException
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class RoomContactsRepository(
    private val contactsDao: ContactsDao
) {
    fun findContactByNameNumber(name: String, number: String): Long{
        val contact = contactsDao.findByNameNumber(name, number) ?: throw AuthenticatorException()
        return contact.id
    }
    fun createContact(newContactData: NewContactData){
        val entity = ContactDbEntity.fromNewContactData(newContactData)
        contactsDao.addContact(entity)
    }
    fun getContactById(id: Long): Contact? {
        return contactsDao.findById(id)?.toContact()
    }
    fun updateNoticeForContactId(id: Long, newNotice: String){
        contactsDao.updateNotice(
            ContactTuple(id, newNotice)
        )
    }
}