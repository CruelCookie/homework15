package com.example.notice

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(
    version = 1,
    entities = [
        ContactDbEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getContactsDao(): ContactsDao
}