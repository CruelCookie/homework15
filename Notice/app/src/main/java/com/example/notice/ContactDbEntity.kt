package com.example.notice

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(
    tableName = "contacts"
)
data class ContactDbEntity(
    @PrimaryKey(autoGenerate = true) val id: Long,
    @ColumnInfo(name = "contact_name") val contactName: String,
    val number: String,
    val notice: String,
    @ColumnInfo(name = "created_at") val createdAt: Long
){
    fun toContact(): Contact = Contact(
        id = id,
        contactName = contactName,
        number = number,
        notice = notice,
        createdAt = createdAt
    )
    companion object{
        fun fromNewContactData(contactData: NewContactData): ContactDbEntity = ContactDbEntity(
            id = 0,
            contactName = contactData.contactName,
            number = contactData.number,
            notice = contactData.notice,
            createdAt = System.currentTimeMillis()
        )
    }
}
