package com.example.notice

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.notice.databinding.ActivityMainBinding
import com.example.notice.databinding.FragmentContactNoticeBinding

class ContactNoticeFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()
    private lateinit var binding: FragmentContactNoticeBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContactNoticeBinding.inflate(inflater)
        val name = dataModel.currentContact.value?.contactName
        val number = dataModel.currentContact.value?.number
        val notice = dataModel.currentContact.value?.notice
        binding.apply {
            tvName.text = name
            tvNumber.text = number
            tvNotice.text = notice
        }
        binding.btUpdNotice.setOnClickListener{
            val newNotice = binding.etNotice.text.toString()
            binding.etNotice.text.clear()
            binding.tvNotice.text = newNotice
            dataModel.roomContactsRepository.value?.updateNoticeForContactId(dataModel.roomContactsRepository.value?.findContactByNameNumber(name!!, number!!)!!, newNotice)
        }
        return binding.root
    }

    companion object {
        @JvmStatic
        fun newInstance() = ContactNoticeFragment()
    }
}