package com.example.notice

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.notice.databinding.ContactItemBinding
import com.example.notice.databinding.FragmentContactListBinding

class ContactAdapter(datamodel: DataModel, listBinding: FragmentContactListBinding): RecyclerView.Adapter<ContactAdapter.contactRecHolder>() {
    val contactRecList = ArrayList<Contact>()
    val dataModel = datamodel
    val binding = listBinding

    class contactRecHolder(item: View): RecyclerView.ViewHolder(item){
        val binding = ContactItemBinding.bind(item)

        fun bind(contact: Contact, datamodel: DataModel, listBinding: FragmentContactListBinding) = with(binding){
            tvIName.text = contact.contactName
            tvINumber.text = contact.number
            tvINotice.text = contact.notice
            linLayout.setOnClickListener {
                datamodel.currentContact.value = contact
                Navigation.findNavController(listBinding.root).navigate(R.id.nav_contListFrag_to_contNotFrag)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): contactRecHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.contact_item, parent, false)
        return contactRecHolder(view)
    }

    override fun onBindViewHolder(holder: contactRecHolder, position: Int) {
        holder.bind(contactRecList[position], dataModel, binding)
    }

    override fun getItemCount(): Int {
        return contactRecList.size
    }

    fun addContact(contact: Contact?){
        if(contact!=null){
            contactRecList.add(contact)
            notifyDataSetChanged()
        }
    }

}