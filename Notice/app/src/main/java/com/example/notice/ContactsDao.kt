package com.example.notice

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import kotlinx.coroutines.flow.Flow

@Dao
interface ContactsDao {

    @Query("SELECT * FROM contacts WHERE contact_Name = :contactName AND number = :number")
    fun findByNameNumber(contactName: String, number: String):ContactDbEntity?

    @Update(entity = ContactDbEntity::class)
    fun updateNotice(contact: ContactTuple)

    @Insert
    fun addContact(contactDbEntity: ContactDbEntity)

    @Query("SELECT * FROM contacts WHERE id = :id")
    fun findById(id: Long):ContactDbEntity?

}