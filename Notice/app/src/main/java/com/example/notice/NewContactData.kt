package com.example.notice

data class NewContactData(
    val contactName: String,
    val number: String,
    val notice: String
)
