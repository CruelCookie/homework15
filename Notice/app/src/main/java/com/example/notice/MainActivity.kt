package com.example.notice

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.util.Log
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.notice.databinding.ActivityMainBinding


private const val PERMISSION_REQUEST = 10

class MainActivity : AppCompatActivity() {
    private val dataModel: DataModel by viewModels()
    private lateinit var binding: ActivityMainBinding
    lateinit var context: Context

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        Repositories.init(applicationContext)
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        context = this

        val permissionStatus = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
        if (permissionStatus == PackageManager.PERMISSION_GRANTED) {
            getContacts()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.READ_CONTACTS), PERMISSION_REQUEST)
        }
        dataModel.roomContactsRepository.value = Repositories.contactsRepository
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            PERMISSION_REQUEST -> {
                if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getContacts()
                } else {
                    Toast.makeText(binding.root.context,"Go to settings and enable the permission", Toast.LENGTH_SHORT).show()
                }
                return
            }
        }
    }

    fun getContacts(){

        var phoneNumber:String? = null;

        val nameList = ArrayList<String>()
        val numbersList = ArrayList<String>()
        //Связываемся с контактными данными и берем с них значения id контакта, имени контакта и его номера:
        val CONTENT_URI = ContactsContract.Contacts.CONTENT_URI;
        val _ID = ContactsContract.Contacts._ID;
        val DISPLAY_NAME = ContactsContract.Contacts.DISPLAY_NAME;
        val HAS_PHONE_NUMBER = ContactsContract.Contacts.HAS_PHONE_NUMBER;

        val PhoneCONTENT_URI = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        val Phone_CONTACT_ID = ContactsContract.CommonDataKinds.Phone.CONTACT_ID;
        val NUMBER = ContactsContract.CommonDataKinds.Phone.NUMBER;

        //StringBuffer output = new StringBuffer();
        val contentResolver = getContentResolver();
        val cursor = contentResolver.query(CONTENT_URI, null,null, null, null);

        //Запускаем цикл обработчик для каждого контакта:
        if (cursor!!.getCount() > 0) {
            //Если значение имени и номера контакта больше 0 (то есть они существуют) выбираем
            //их значения в приложение привязываем с соответствующие поля "Имя" и "Номер":
            while (cursor.moveToNext()) {
                val contact_id = cursor.getString(cursor.getColumnIndexOrThrow( _ID ));
                val name = cursor.getString(cursor.getColumnIndexOrThrow(DISPLAY_NAME));
                val hasPhoneNumber = Integer.parseInt(cursor.getString(cursor.getColumnIndexOrThrow(HAS_PHONE_NUMBER)));

                //Получаем имя:
                if (hasPhoneNumber > 0) {
                    val phoneCursor = contentResolver.query(PhoneCONTENT_URI, null, Phone_CONTACT_ID + " = ?", Array<String>(1) { contact_id }, null)
                    //и соответствующий ему номер:
                    while (phoneCursor!!.moveToNext()) {
                        phoneNumber = phoneCursor.getString(phoneCursor.getColumnIndexOrThrow(NUMBER));
                    }
                }
                if (phoneNumber!=null && name!=null)
                {
                    Repositories.contactsRepository.createContact(NewContactData(name, phoneNumber, ""))
                    Log.i("CONTACTS"," ${Repositories.contactsRepository.getContactById(Repositories.contactsRepository.findContactByNameNumber(name, phoneNumber))}  - $phoneNumber")
                    numbersList.add(phoneNumber)
                    nameList.add(name)
                }
            }
        }
        dataModel.names.value = nameList
        dataModel.numbers.value = numbersList
    }
}