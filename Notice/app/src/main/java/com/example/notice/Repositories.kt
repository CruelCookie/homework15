package com.example.notice

import android.content.Context
import androidx.room.Room

object Repositories {
    private lateinit var applicationContext: Context

    private val database: AppDatabase by lazy<AppDatabase>{
        Room.databaseBuilder(applicationContext, AppDatabase::class.java, "database.db").allowMainThreadQueries().build()
    }
    val contactsRepository by lazy{
        RoomContactsRepository(database.getContactsDao())
    }
    fun init(context: Context) {
        applicationContext = context
    }
}