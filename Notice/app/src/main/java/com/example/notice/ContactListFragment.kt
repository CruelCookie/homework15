package com.example.notice

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.notice.databinding.FragmentContactListBinding

class ContactListFragment : Fragment() {
    private val dataModel:DataModel by activityViewModels()
   private lateinit var binding: FragmentContactListBinding
   private lateinit var adapter: ContactAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentContactListBinding.inflate(inflater)
        adapter = ContactAdapter(dataModel, binding)
        init()
        return binding.root
    }

    private fun init(){
        binding.apply {
            noticeRec.layoutManager = LinearLayoutManager(this.root.context)
            noticeRec.adapter = adapter
            val names = dataModel.names.value
            val numbers = dataModel.numbers.value
            for(i in 0..dataModel.names.value?.size!!-1){
                adapter.addContact(dataModel.roomContactsRepository.value?.getContactById(dataModel.roomContactsRepository.value?.findContactByNameNumber(names?.get(i) ?: "Oleg", numbers?.get(i) ?: "Oleg")!!))
            }
        }
    }
    companion object {
        @JvmStatic
        fun newInstance() = ContactListFragment()
    }
}