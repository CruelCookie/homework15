package com.example.notice

import androidx.room.ColumnInfo

data class ContactTuple(
    @ColumnInfo(name = "id") val id: Long,
    @ColumnInfo(name = "notice") val notice: String

)