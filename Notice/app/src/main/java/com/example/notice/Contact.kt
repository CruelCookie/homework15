package com.example.notice

data class Contact(
    val id: Long,
    val contactName: String,
    val number: String,
    val notice: String,
    val createdAt: Long
)
